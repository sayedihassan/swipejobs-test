import RequestStatus from '@utils/constants/requestStatus';
import { useEffect, useState, useRef } from 'react';

interface fetchProps {
  url?: string;
  cache?: boolean;
}

type Cache = { [url: string]: unknown };

function useFetch({ url = '', cache = false }: fetchProps) {
  const cached = useRef<Cache>({});
  const [query, setQuery] = useState<string>(url);
  const [response, setResponse] = useState<unknown>();
  const [status, setStatus] = useState<RequestStatus>(RequestStatus.IDLE);
  const [message, setMessage] = useState<string>('');

  useEffect(() => {
    const abortController = new AbortController();
    (async () => {
      if (!query) return;
      try {
        if (cache && cached.current[url]) {
          const data = cached.current[url];

          setResponse(data);
        } else {
          setStatus(RequestStatus.LOADING);

          const result = await fetch(query, {
            signal: abortController.signal,
          });
          const data = await result.json();

          if (data.success === false) {
            throw new Error(data.message);
          }

          if (cache) {
            cached.current[url] = data;
          }

          setResponse(data);
        }
        
        setStatus(RequestStatus.SUCCESS);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      } catch (e: any) {
        if (e.name === 'AbortError') {
          setStatus(RequestStatus.IDLE);
        }else{
          setStatus(RequestStatus.ERROR);
          setMessage(e.message);
        }
      }
    })();
    return () => {
      abortController.abort();
    };
  }, [cache, query, url]);

  return { response, message, status, setQuery };
}

export default useFetch;
