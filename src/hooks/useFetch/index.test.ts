import 'whatwg-fetch';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import { renderHook, waitFor } from '@testing-library/react';
import RequestStatus from '@utils/constants/requestStatus';
import useFetch from './index';
import { act } from 'react-dom/test-utils';

const mockUrl = 'https://test.swipejobs.com/api/worker/123/matches';
const mockResponse = [{jobId:'1'},{jobId:'2'}];
const server = setupServer(
    rest.get(mockUrl, (req, res, ctx) => {
      return res(ctx.json(mockResponse));
    })
);

describe('useFetch', () => {
  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  it('returns "idle" status when url is not passed', () => {
    const { result } = renderHook(() => useFetch({}));
    
    expect(result.current.status).toEqual(RequestStatus.IDLE);
  });

  it('returns "loading" status when data is fetching ', async() => {
    const { result  } = renderHook(() => useFetch({}));

    expect(result.current.status).toEqual(RequestStatus.IDLE);

    act(() => {
      result.current.setQuery(mockUrl);
    });

    expect(result.current.status).toEqual(RequestStatus.LOADING);
  });

  it('returns "error" status when data fetching is unsuccessful from server', async() => {
    server.use(
        rest.get(mockUrl, (req, res, ctx) => {
          return res(ctx.status(500,'server error'));
        }),
    );
    
    const { result,  } = renderHook(() => useFetch({url:mockUrl}));

    await waitFor(() => expect(result.current.status).toEqual(RequestStatus.LOADING));

    await waitFor(() => expect(result.current.status).toEqual(RequestStatus.ERROR));
  });
  
  describe('when fetch is a success', () => {
    it('returns "success" status after data fetching is complete', async() => {
      const { result,  } = renderHook(() => useFetch({url:mockUrl}));

      await waitFor(() => expect(result.current.status).toEqual(RequestStatus.LOADING));

      await waitFor(() => expect(result.current.status).toEqual(RequestStatus.SUCCESS));
      
      expect(result.current.response).toEqual(mockResponse);
    });

    it('returns "error" status when data fetching is success with a "success" field false', async() => {
      server.use(
          rest.get(mockUrl, (req, res, ctx) => {
            return res(ctx.json({success:false, message:'fetch failed'}));
          }),
      );
      const { result } = renderHook(() => useFetch({url:mockUrl}));

      await waitFor(() => expect(result.current.status).toEqual(RequestStatus.LOADING));

      await waitFor(() => expect(result.current.status).toEqual(RequestStatus.ERROR));

      expect(result.current.message).toEqual('fetch failed');
    });
  });
});