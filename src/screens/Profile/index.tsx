import React from 'react';
import classNames from 'classnames/bind';
import { useNavigate } from 'react-router-dom';
import { useUserContext } from '@appContext/UserContext';
import { formatPhoneNumber } from '@utils/helpers/phone';
import Button from '@components/UI/Button';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

export default function Profile() {
  const { userData } = useUserContext();
  const navigate = useNavigate();

  if (!userData) return <></>;
  return (
    <div className={cx('profile-wrapper')}>
      <h2>{userData.firstName} {userData.lastName}</h2>
      <UserDetails title='Email' value={userData.email} />
      <UserDetails title='Phone' value={formatPhoneNumber(userData.phoneNumber)} />
      <UserDetails title='Address' value={userData.address.formattedAddress} />
      <UserDetails title='Max Job Distance' value={`${userData.maxJobDistance} miles`} />
      <div className={cx('action-wrapper')}>
        <Button onClick={() => navigate('/')} label='View Jobs' />
      </div>
    </div>
  );
}

interface UserDetailsProps {
  title: string;
  value: string | number | React.ReactNode;
}

function UserDetails({ title, value }: UserDetailsProps) {
  return (
    <div className={cx('info')}>
      <div className={cx('info-title')}>{title}</div>
      <div className={cx('info-value')}>{value}</div>
    </div>
  );
}