import React from 'react';
import classNames from 'classnames/bind';
import { useNavigate } from 'react-router-dom';
import Button from '@components/UI/Button';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

export default function PageNotFound() {
  const navigate = useNavigate();

  return (
    <div className={cx('page-not-found')}>
      <h1>Sorry, this page isn't available</h1>
      <Button label='Back to Jobs' onClick={()=>navigate('/')} />
    </div>
  );
}
