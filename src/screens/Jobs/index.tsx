import React, { useEffect } from 'react';
import classNames from 'classnames/bind';
import { isEmpty } from 'lodash';
import { Job } from '@appTypes/Job';
import { useUserContext } from '@appContext/UserContext';
import { useJobContext } from '@appContext/JobContext';
import { useFetch } from '@hooks';
import RequestStatus from '@utils/constants/requestStatus';
import { Spinners } from '@components/UI';
import JobsList from '@components/JobsList';
import JobView from './JobView';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

export default function Jobs() {
  const { jobData, setJobData } = useJobContext();
  const { userData } = useUserContext();
  const { response, status, setQuery } = useFetch({ url: '' });
  
  useEffect(() => {
    setQuery(`https://test.swipejobs.com/api/worker/${userData?.workerId}/matches`);
    return ()=> setJobData(null);
  },[setJobData, setQuery, userData?.workerId]);

  return (
    <>
      {(status===RequestStatus.SUCCESS&& isEmpty(jobData) && response) &&
        <>
          <div className={cx('page-stats')}>Showing {(response as Job[]).length} jobs</div>
          <JobsList jobs={response as Job[]} />
        </>
      }
      {status===RequestStatus.LOADING && <div className={cx('pad-wrapper')}><Spinners.FadingCircle theme="dark" /></div>}
      {status===RequestStatus.ERROR && <div className={cx('pad-wrapper')}>Error fetching jobs..</div>}
      {(!isEmpty(jobData)) && <JobView />}
    </>
  );
}
