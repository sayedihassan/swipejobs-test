import React, { useState } from 'react';
import classNames from 'classnames/bind';
import { useJobContext } from '@appContext/JobContext';
import { useUserContext } from '@appContext/UserContext';
import { useFetch } from '@hooks';
import RequestStatus from '@utils/constants/requestStatus';
import currencyMask from '@utils/helpers/currency';
import { Icons } from '@utils/constants/icons';
import { roundDecimals } from '@utils/helpers/number';
import { dateFormatter } from '@utils/helpers/datetime';
import { formatPhoneNumber } from '@utils/helpers/phone';
import Button from '@components/UI/Button';
import Info from '@components/Info';
import PageStats from '@components/PageStats';
import JobDetails from '@components/JobDetails';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

export default function JobView() {
  const { jobData, setJobData } = useJobContext();
  const { userData } = useUserContext();
  const { status, message, setQuery } = useFetch({ url: '' });
  const [jobStatus, setJobStatus] = useState('');

  const onAccept = () => {
    setQuery(`https://test.swipejobs.com/api/worker/${userData?.workerId}/job/${jobData?.jobId}/accept`);
    setJobStatus('accepted');
  };

  const onReject = () => {
    setQuery(`https://test.swipejobs.com/api/worker/${userData?.workerId}/job/${jobData?.jobId}/reject`);
    setJobStatus('rejected');
  };

  const onClearJob = () => {
    setJobData(null);
  };
  
  if (!jobData) return <></>;
  return (
    <div className={cx('job-wrapper')}>
      <PageStats><span className={cx('back-button')} onClick={onClearJob}><span>&#10229;</span> Back to jobs</span></PageStats>
      <div className={cx('job-banner')}><img src={jobData.jobTitle.imageUrl} alt={jobData.jobTitle.name} /></div>
      <div className={cx('job-highlight')}>
        <JobHighlight title='Distance' value={`${roundDecimals(jobData.milesToTravel)} miles`} />
        <JobHighlight title='Hourly Rate' value={hourlyRateMask(currencyMask(jobData.wagePerHourInCents))} />
      </div>
      <div className={cx('job-content')}>
        <div className={cx('sub-content')}>
          <JobDetails title='Shift Dates' icon={Icons.Calendar}>
            {jobData.shifts.map((item, index) => {
              return (<div key={index}>{dateFormatter(item.startDate, item.endDate)}</div>);
            })}
          </JobDetails>
          <JobDetails
            title='Locaiton'
            icon={Icons.Pin}
            url={encodeURI(`https://www.google.com/maps/search/${jobData.company.address.formattedAddress}`)}
            urlLabel='Get Direction >'
          >
            <div>{jobData.company.address.formattedAddress}</div>
            <div className={cx('location-distance')}>{roundDecimals(jobData.milesToTravel)} miles from your job search</div>
          </JobDetails>
        </div>
        <JobDetails title='Requirements' icon={Icons.Tools}>
          {jobData.requirements?.map((item, index) => {
            return (<div key={index}>- {item}</div>);
          })}
        </JobDetails>
        <JobDetails title='Report To' icon={Icons.User} noBorder>
          <div>{jobData.company.reportTo.name} {formatPhoneNumber(jobData.company.reportTo.phone)}</div>
        </JobDetails>
        <div className={cx('action-wrapper')}>
          {
            !(status===RequestStatus.ERROR||status===RequestStatus.SUCCESS) &&
            <>
              <Button isLoading={status===RequestStatus.LOADING} variant='outlined' label='No Thanks' onClick={onReject} />
              <Button isLoading={status===RequestStatus.LOADING} label={`I'll Take it`} onClick={onAccept} />
            </>
          }
          {status===RequestStatus.ERROR && <Info message={message} type='error' />}
          {status===RequestStatus.SUCCESS && <Info message={`Job ${jobStatus}`} type='success' />}
        </div>
      </div>
    </div >
  );
  function hourlyRateMask(rate: string) {
    return (<div className={cx('hourly-rate')}>{rate}</div>);
  }
}

interface JobHighlightProps {
  title: string;
  value: string | number | React.ReactNode;
}
function JobHighlight({ title = '', value = '' }: JobHighlightProps) {
  return (
    <div className={cx('info')}>
      <div className={cx('info-title')}>{title}</div>
      <div className={cx('info-value')}>{value}</div>
    </div>
  );
}
