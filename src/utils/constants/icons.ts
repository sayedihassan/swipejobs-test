export enum Icons {
  User = 'user',
  Pin = 'pin',
  Calendar = 'calendar',
  Tools = 'tools',
  ArrowRight = 'arrow-right',
}