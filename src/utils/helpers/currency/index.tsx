

/**
 * Formats cents to dollar with currency mask
 *
 * @param {number} cents
 * @param {string} currency
 * @returns {string} i.e $9.63
 */
const currencyMask = (cents=0, currency = 'USD') => {
  return (cents / 100).toLocaleString('en-US', { style: 'currency', currency: currency });
};

export default currencyMask;