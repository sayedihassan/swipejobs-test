import currencyMask from './index';

describe('currencyMask', () => {
  it('return correct amount when cents is zero', () => {
    const amount = currencyMask(0, 'USD');

    expect(amount).toEqual('$0.00');
  });

  it('return correct amount when cents is in hundreds', () => {
    const amount = currencyMask(105, 'USD');

    expect(amount).toEqual('$1.05');
  });

  it('return correct amount when cents is in thousands', () => {
    const amount = currencyMask(40563, 'USD');

    expect(amount).toEqual('$405.63');
  });
});
