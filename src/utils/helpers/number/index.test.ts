import { roundDecimals } from './index';

describe('roundDecimals', () => {
  it('returns correct rounded value when it\'s a whole number', () => {
    const roundedNumber = roundDecimals(2);

    expect(roundedNumber).toEqual(2);
  });

  it('returns correct rounded value when one decimal point', () => {
    const roundedNumber = roundDecimals(3.6);
    
    expect(roundedNumber).toEqual(3.6);
  });

  it('returns correct rounded value when multiple decimal point', () => {
    const roundedNumber = roundDecimals(5.568);
    
    expect(roundedNumber).toEqual(5.57);
  });
});
