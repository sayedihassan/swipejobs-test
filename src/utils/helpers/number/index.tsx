
/**
 * Formats a number using fixed-point notation
 *
 * @param {number} num
 * @returns {number} 
 */
export const roundDecimals = (num:number) => {
  return parseFloat(num.toFixed(2));
};
