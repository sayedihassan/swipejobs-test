import { formatPhoneNumber } from './index';

describe('formatPhoneNumber', () => {
  it('returns null when invalid phone number', () => {
    const phoneNumber = formatPhoneNumber('');

    expect(phoneNumber).toBeNull();
  });
  
  it('returns correct formatted phone number with country code', () => {
    const phoneNumber = formatPhoneNumber('+12130010012');

    expect(phoneNumber).toEqual('+1 (213) 001-0012');
  });

  it('returns correct formatted phone number', () => {
    const phoneNumber = formatPhoneNumber('2130010012');

    expect(phoneNumber).toEqual('(213) 001-0012');
  });
});
