import { format, parseISO } from 'date-fns';

/**
 * Formats start date and end date to "MMM d, E hh:mm a - hh:mm a"
 *
 * @param {string} startDate
 * @param {string} endDate
 * @returns {string} Date and time formatted i.e Sep 5, Thu 07:00 AM - 03:00 PM
 */
export const dateFormatter = (startDate: string, endDate: string) => {
  try {
    return `${format(parseISO(startDate), 'MMM d, E hh:mm a')} - ${format(parseISO(endDate), 'hh:mm a')}`;
  } catch (error) {
    return '';
  }
};
