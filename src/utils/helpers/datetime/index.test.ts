import { dateFormatter } from './index';

describe('dateFormatter', () => {
  it('returns correct formatted datetime when dates are correct', () => {
    const datetime = dateFormatter('2019-09-04T21:00:00Z','2019-09-05T05:00:00Z');

    expect(datetime).toEqual('Sep 5, Thu 07:00 AM - 03:00 PM');
  });

  it('returns empty string when date is corrupted', () => {
    const datetime = dateFormatter('2019-09-04T21:00:00Z','');

    expect(datetime).toEqual('');
  });
});
