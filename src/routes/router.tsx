import { createBrowserRouter } from 'react-router-dom';
import { MainLayout } from '@layouts';
import Jobs from '@screens/Jobs';
import Profile from '@screens/Profile';
import PageNotFound from '@screens/PageNotFound';

const router = createBrowserRouter([
  {
    element: <MainLayout />,
    children: [
      {
        path: '/',
        element: <Jobs />,
      },
      {
        path: '/profile',
        element: <Profile />
      },
      {
        path: '*',
        element: <PageNotFound />
      }
    ],
  },
]);
export default router;