import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import {
  RouterProvider,
} from 'react-router-dom';
import { router } from '@routes';
import './index.scss';
import { JobContextProvider } from './context/JobContext';
import { UserContextProvider } from '@appContext/UserContext';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
    <React.StrictMode>
      <UserContextProvider>
        <JobContextProvider>
          <RouterProvider router={router} />
        </JobContextProvider>
      </UserContextProvider>
    </React.StrictMode>
);

reportWebVitals();
