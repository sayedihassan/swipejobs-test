import { Job } from '@appTypes/Job';
import React, { createContext, useState } from 'react';


type JobContextProps = {
  jobData?: Job|null|undefined;
  setJobData: (value: Job|null|undefined) => void;
};

const initialState = {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setJobData: () => {}
};

export const JobContext = createContext<JobContextProps>(initialState);

type JobContextProviderProps = {
  children: React.ReactNode;
};

export const JobContextProvider = ({ children }: JobContextProviderProps) => {

  const [jobData, setJobData] = useState<Job|null|undefined>();

  return (
    <JobContext.Provider value={{ jobData, setJobData }}>
      {children}
    </JobContext.Provider>
  );
};

export const useJobContext = () => React.useContext(JobContext);