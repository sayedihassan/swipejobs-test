import { User } from '@appTypes/User';
import React, { createContext, useState } from 'react';


type UserContextProps = {
  userData?: User|undefined;
  setUserData: (value: User|undefined) => void;
};

const initialState = {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setUserData: () => {}
};

export const UserContext = createContext<UserContextProps>(initialState);

type UserContextProviderProps = {
  children: React.ReactNode;
};

export const UserContextProvider = ({ children }: UserContextProviderProps) => {

  const [userData, setUserData] = useState<User|undefined>();

  return (
    <UserContext.Provider value={{ userData, setUserData }}>
      {children}
    </UserContext.Provider>
  );
};

export const useUserContext = () => React.useContext(UserContext);