import React, { useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import classNames from 'classnames/bind';
import { isEmpty } from 'lodash';
import { User } from '@appTypes/User';
import { useUserContext } from '@appContext/UserContext';
import { useFetch } from '@hooks';
import Header from '@components/Header';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

export default function MainLayout() {
  const { setUserData } = useUserContext();
  const { response } = useFetch({ url: 'https://test.swipejobs.com/api/worker/7f90df6e-b832-44e2-b624-3143d428001f/profile', cache: true });

  useEffect(() => {
    if (!isEmpty(response)) {
      setUserData(response as User);
    }
  }, [response, setUserData]);

  return (
    <>
      <Header />
      <div className={cx('main-wrapper')}>
        <div className={cx('content')}>
          <Outlet />
        </div>
      </div>
    </>
  );
}
