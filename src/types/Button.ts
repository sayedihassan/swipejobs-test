export type ButtonVariants = 'outlined' | 'contained';
export type ButtonSize = 'sm' | 'md';
