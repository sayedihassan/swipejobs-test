interface Shift {
  startDate: string;
  endDate: string;
}

export interface Job {
  jobId: string;
  jobTitle: {
    name: string;
    imageUrl: string;
  };
  company: {
    name: string;
    address: {
      formattedAddress: string;
    },
    reportTo:{
      name: string;
      phone: string;
    }
  };
  shifts: Shift[];
  requirements: string[];
  wagePerHourInCents: number;
  milesToTravel: number;
}
