import React from 'react';
import classNames from 'classnames/bind';
import { Job } from '@appTypes/Job';
import { useJobContext } from '@appContext/JobContext';
import { useBreakpoints } from '@hooks';
import currencyMask from '@utils/helpers/currency';
import { roundDecimals } from '@utils/helpers/number';
import { Breakpoints } from '@utils/constants/breakpoints';
import Button from '@components/UI/Button';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);


export default function JobListItem({ job }: { job: Job }) {
  const { setJobData } = useJobContext();
  const breakpoints = useBreakpoints();

  const onViewDetails = () => {
    setJobData(job);
  };
  return (
    <div className={cx('job-wrapper')}>
      <div className={cx('left-wrapper')}>
        <div className={cx('title')}>{job.jobTitle.name}</div>
        <div className={cx('company')}>{job.company.name}</div>
        <div className={cx('details-wrapper')}>
          <JobDetails title="Distance" value={`${roundDecimals(job.milesToTravel)} mi`} />
          <JobDetails title="Shifts" value={job.shifts.length} />
        </div>
      </div>
      <div className={cx('right-wrapper')}>
        <div className={cx('rate')}>{currencyMask(job.wagePerHourInCents)}<span className={cx('rate-hr')}>/hr</span></div>
        <div className={cx('button-wraper')}><Button size={ [Breakpoints.Tablet,Breakpoints.Mobile
        ].includes(breakpoints) ? 'sm' : 'md'} label="View Details" onClick={() => onViewDetails()} /></div>
      </div>
    </div>
  );
}

interface JobDetails {
  title: string;
  value: string | number;
}

function JobDetails({ title = '', value = '' }: JobDetails) {
  return (
    <div className={cx('info')}>
      <div className={cx('info-title')}>{title}</div>
      <div className={cx('info-value')}>{value}</div>
    </div>
  );
}
