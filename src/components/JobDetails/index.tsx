import React from 'react';
import classNames from 'classnames/bind';
import { useBreakpoints } from '@hooks';
import { Breakpoints } from '@utils/constants/breakpoints';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

interface JobDetailsProps {
  title: string;
  icon: string;
  url?: string;
  urlLabel?: string;
  children: React.ReactNode;
  noBorder?: boolean;
}
const JobDetails = ({ title = '', icon = '', children, url = '', urlLabel = '', noBorder = false }: JobDetailsProps) => {
  const breakpoints = useBreakpoints();
  return (
    <div className={cx('details', { 'noBorder': noBorder })}>
      <div className={cx('icon-wrapper')}>
        <span className={`icon-${icon}`}></span>
      </div>
      <div className={cx('details-wrapper')}>
        <div className={cx('title-wrapper')}>
          {title}
        </div>
        {children}
      </div>
      {url &&
        <div className={cx('link-wrapper')}>
          <a href={url} target='_blank'>{ [Breakpoints.Tablet,Breakpoints.Mobile].includes(breakpoints) ? <span className='icon-arrow-right'></span> : urlLabel}</a>
        </div>
      }
    </div>
  );
};
export default JobDetails;