import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

function PageStats({ children }: { children: React.ReactNode }) {
  return (
    <div className={cx('page-stats')}>
      {children}
    </div >
  );
}

export default PageStats;