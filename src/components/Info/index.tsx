import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

interface infoProps { 
  message: string;
  type: 'success' | 'error';
}

const Info = ({ message, type = 'success' }: infoProps) => {
  return (
    <div className={cx('alert',{
      'alert-success': type === 'success',
      'alert-error': type === 'error'
    })}>
      {message}
    </div >
  );
};
export default Info;