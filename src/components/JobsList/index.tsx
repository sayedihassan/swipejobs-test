import React from 'react';
import classNames from 'classnames/bind';
import { Job } from '@appTypes/Job';
import JobListItem from '@components/JobListItem';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

interface JobsList {
  jobs: Job[];
}

export default function JobsList({ jobs = [] }: JobsList) {
  return (
    <div className={cx('jobs-wrapper')}>
      {jobs.map((item: Job, index) => {
        return (
          <React.Fragment key={index}>
            <JobListItem job={item} />
          </React.Fragment>
        );
      })}
    </div>
  );
}
