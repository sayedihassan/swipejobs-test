import React from 'react';
import classNames from 'classnames/bind';
import { Link } from 'react-router-dom';
import { useUserContext } from '@appContext/UserContext';
import swipeLogo from '@assets/img/sj-logo-sj-white.svg';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

function Header() {
  const { userData } = useUserContext();
  
  const userName = userData? `${userData.firstName} ${userData.lastName}`:'';

  return (
    <header className={cx('header-wrapper')}>
      <div className={cx('logo-wrapper')}>
        <Link to='/'><img className={cx('logo')} src={swipeLogo} alt='swipejobs' /></Link>
      </div>
      <div className={cx('right-wrapper')}>
        <div className={cx('profile-name')}>
          <Link to='/profile'>{userName}</Link>
        </div>
      </div>
    </header >
  );
}

export default Header;