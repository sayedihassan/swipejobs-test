import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.scss';

const cx = classNames.bind(styles);

interface defaultProps {
  label: string;
  type?: 'submit' | 'button';
  variant?: 'outlined' | 'contained';
  size?: 'sm' | 'md';
  isLoading?: boolean;
  onClick?: () => void;
}

export default function Button({ label = '', type = 'button', variant = 'contained', size = 'md', isLoading, onClick }: defaultProps) {
  return (
    <button
      type={type}
      className={cx('button', {
        'button-outlined': variant === 'outlined',
        'button-sm': size === 'sm',
      })}
      onClick={onClick}
      disabled={isLoading}
    >{label}</button>
  );
}
